document.getElementById("Button").addEventListener("click", function() {
    alert("Sorry! This button is not active right now.");
});

const icon = document.querySelector(".icon");
const menu = document.querySelector(".nav__menu")

icon.addEventListener("click", () => {
    menu.classList.toggle("show");
})